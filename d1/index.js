console.log('Hellow World?');

// [SECTION] While Loop

	// - A while loop takes in an expression/condition
	 

	// let count = 5;

	// while(count !== 0){

	// 	console.log('While: ' + count);

	// 	count--;
	// };

		// Another Simple Example

		// let grade = 1;

		// while(grade <= 5){
		// 	console.log("I'am a grade " + grade + " student!");
		// 	// Without the second statement, this will create an infinite loop

		// 	grade++;
		// 	// Thus, putting a second statement is necessary in preventing an infinite loop
		// };

		/*
		Syntax:
			while(expression/condition){
				statement
			}
		*/	

			// Another Example

				let a = 1;
				let b = 5;

				while(a <= b){
					console.log('Apple ' + a);
					a++;
				};

// [SECTION] Do While Loop

	// - A do-while loop works a lot like the while loop. But unlike while loops, do-while loops guarantee that the code will be executed at least once.

		/*
		Syntax:
			do {
				statement
			} while (expression/condition)
		*/

		// let number = Number(prompt('Give me a number'));
		// console.log(number);
		// 	console.log(typeof number);

		// 	do {

		// 		// The current value of number is printed out
		// 		console.log('Do While: ' + number);

		// 		// Increases the value of number by 1 after every iteration to stop the loop when it reaches 10 or greater
		// 			// number = number + 1
		// 		number += 1;
		// 	} while (number < 10);

				// Another Example

				let c = 1;
				let d = 5;

					do {
						console.log('Carrots ' + c);
						c++;
					} while (c <= d);

// [SECTION] For Loop

	// - A for loop is more flexible that while and do-while loops. It consists of three parts:
			// 1. The 'initialization' value that will track the progression of the loop
			// 2. The 'expression/condition' that will be evaluated which will determined whether the loop will run one more time.
			// 3. The 'finalExpression' indicates how to advance the loop.

				//let f = 5;

				for(let e = 1; e <= 5; ++e){
					console.log('Eggplant ' + e);
				};

		/*
		Syntax:
			for(initialization; expression/condition; finalExpression){
				statement;
			}

		*/	
			// for (let count = 0; count <= 10; count++){
			// 	console.log(count);
			// };

		// let myStrings = 'Carlo';
		// console.log(myStrings);
		// console.log(myStrings.length);

		// 	console.log(myStrings[0]);
		// 	console.log(myStrings[1]);
		// 	console.log(myStrings[2]);
		// 	console.log(myStrings[3]);
		// 	console.log(myStrings[4]);

		// let x = 0;
		
		// for(x ; x < myStrings.length; x++){

		// 	// The current value of myStrings is printed out using it's index value
		// 	console.log(myStrings[x]);
		// };

// Create a string named 'myName' with a value of your name;
		let myName = 'CARLO';

		for(let i = 0; i < myName.length; i++){
			if (
				myName[i].toLowerCase() == 'a' ||
				myName[i].toLowerCase() == 'i' ||
				myName[i].toLowerCase() == 'u' ||
				myName[i].toLowerCase() == 'e' ||
				myName[i].toLowerCase() == 'o' 
			){
				console.log(3);
			} else {
				console.log(myName[i])
			}
		}


// [SECTION] Continue and Break Statements

	// - The 'Continue' statement allows the code to go to the next iteration of the loop without finishing the execution of all statements in a code block.
	// - The 'break' statements is used to terminate the current loop once a match has been found

		// Simple Example of Break

		for(let g = 1; g <= 12; g++){
			if (g === 8)
				break;

			console.log('Grapes ' + g);
		};


		// Simple Example of Continue

		for(let i = 1; i <= 12; i++){
			if (i === 6)
				continue;

			console.log('Indian Mango ' + i);
		};

		for (let count = 0; count <= 20; count++){
			if (count % 2 === 0) {
				continue;
			}

			console.log('Continue and Break: ' + count);

			// If the current value of count is greater than 13
			if (count > 20) { 

				// Tells the code to terminate/stop the loop even if the expression/condition of the loop defines that it should executes so long as the value of count is less than or equal to 20.
				break;
			}
		}

	let name = 'alexandro';

		for (let i = 0; i < name.length; i++){
			console.log(name[i]);

			// If the vowel is equal to a, continue to the next iteration of the loop
			if(name[i].toLowerCase() === 'a'){
				console.log('Continue to the next iteration');
				continue;
			}

			// If the current letter is equal to d, stop the loop
			if (name[i] == 'd'){
				break;
			}
		}
